#!/usr/bin/python

from setuptools import setup, find_packages

setup(
    name="authserv",
    version="0.1.2",
    description="Authentication server",
    author="Autistici/Inventati",
    author_email="info@autistici.org",
    url="https://git.autistici.org/ai/authserv",
    install_requires=["gevent", "python-ldap", "Flask", "pylibmc", "ipaddr"],
    setup_requires=[],
    zip_safe=False,
    packages=find_packages(),
    package_data={},
    entry_points={
        'console_scripts': [
          'ai-auth-server = authserv.server:main',
          ],
        }
    )

