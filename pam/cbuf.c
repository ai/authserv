
#include <stdlib.h>
#include <memory.h>
#include "cbuf.h"


void cbuf_init(struct cbuf *cbuf, size_t alloc) {
  cbuf->buf = (char *)malloc(1);
  cbuf->buf[0] = '\0';
  cbuf->alloc = cbuf->size = 0;
}

void cbuf_free(struct cbuf *cbuf) {
  free(cbuf->buf);
}

void cbuf_append(struct cbuf *cbuf, const char *data, size_t size) {
  size_t new_alloc = cbuf->alloc + size;
  cbuf->buf = (char *)realloc(cbuf->buf, new_alloc + 1);
  memcpy(cbuf->buf + cbuf->size, data, size);
  cbuf->alloc = cbuf->size = new_alloc;
  cbuf->buf[cbuf->size] = '\0';
}
