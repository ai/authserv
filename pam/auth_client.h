#ifndef __libauthclient_authclient_h
#define __libauthclient_authclient_h 1

#include <curl/curl.h>

struct auth_client;
typedef struct auth_client* auth_client_t;

#define AC_OK                           0
#define AC_ERR_AUTHENTICATION_FAILURE  -1
#define AC_ERR_OTP_REQUIRED            -2
#define AC_ERR_BAD_RESPONSE            -3
#define AC_ERR_FILE_NOT_FOUND          -4
#define AC_ERR_NO_SERVERS              -5
#define AC_ERR_CURL_BASE             -100
#define auth_client_err_to_curl(e)     (-(e)+(AC_ERR_CURL_BASE))
#define auth_client_err_from_curl(e)   ((AC_ERR_CURL_BASE)-(e))

/*
 * Create a new auth client instance.
 *
 * @param service Service name
 * @param servers A comma-separated list of host:port auth server
 * addresses
 */
auth_client_t auth_client_new(const char *service, const char *servers);

/*
 * Free all resources associated with an auth client instance.
 */
void auth_client_free(auth_client_t ac);

/*
 * Return a human readable error string.
 *
 * @param err Error code returned by one of the auth_client_* methods
 */
const char *auth_client_strerror(int err);

/*
 * Set request verbosity.
 *
 * If verbose is set to 1, libcurl will dump the outbound requests to
 * standard error.
 *
 * @param ac Auth client
 * @param verbose Verbosity: 1 to enable, 0 to disable
 */
void auth_client_set_verbose(auth_client_t ac, int verbose);

/*
 * Set up SSL credentials, and enable HTTPS.
 *
 * @param ac Auth client
 * @param ca_file Path to the CA certificate (PEM format)
 * @param crt_file Path to the client certificate (PEM format)
 * @param key_file Path to the client certificate key
 */
int auth_client_set_certificate(auth_client_t ac,
                                const char *ca_file,
                                const char *crt_file,
                                const char *key_file);

/*
 * Authenticate a user.
 *
 * @param ac Auth client
 * @param username Username
 * @param password Password
 * @param otp_token OTP token, if present (as a string)
 * @param source_ip Source IP of the user, where available
 * @param shard Shard identifier (as a string)
 */
int auth_client_authenticate(auth_client_t ac,
                             const char *username,
                             const char *password,
                             const char *otp_token,
                             const char *source_ip,
			     const char *shard);

#endif
