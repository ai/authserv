// Tests for cbuf.c.

#include <stdlib.h>
#include <string>
#include "gtest/gtest.h"
extern "C" {
#include "cbuf.h"
}

TEST(Cbuf, Append) {
  struct cbuf c;
  cbuf_init(&c, 0);
  cbuf_append(&c, "hello, ", 7);
  cbuf_append(&c, "world", 5);
  std::string a(c.buf);
  EXPECT_EQ("hello, world", a);
  cbuf_free(&c);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
