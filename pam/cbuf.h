#ifndef __libauthclient_cbuf_h
#define __libauthclient_cbuf_h 1

#include <sys/types.h>

/*
 * A dynamically sized memory buffer that can be appended to, and will
 * grow accordingly. It is optimized to perform well for a specific
 * size (the initial allocation).
 */

struct cbuf {
  char *buf;
  size_t alloc, size;
};

void cbuf_init(struct cbuf *cbuf, size_t alloc);
void cbuf_free(struct cbuf *cbuf);
void cbuf_append(struct cbuf *cbuf, const char *data, size_t size);

#endif
