// Tests for auth_client.c.

#include <string>
#include <stdlib.h>
#include "gtest/gtest.h"
extern "C" {
#include "auth_client.h"
}

static const char *server = NULL;

// Must match what is in authserv/test/test_integration.py.
static const char *test_password = "fagiano& +\"% b^rb$g~=|{;\\i";

static const char *ssl_ca = "../authserv/test/testca/public/ca.pem";
static const char *ssl_cert = "../authserv/test/testca/public/certs/client.pem";
static const char *ssl_key = "../authserv/test/testca/private/client.key";
static const char *ssl_bad_ca = "../authserv/test/testca-bad/ca.pem";
static const char *ssl_bad_cert = "../authserv/test/testca-bad/certs/client.pem";
static const char *ssl_bad_key = "../authserv/test/testca-bad/private/client.key";

TEST(AuthClientCurlInterface, ErrorConversion) {
  int curl_err = 35;
  int err = auth_client_err_from_curl(curl_err);
  int translated = auth_client_err_to_curl(err);
  EXPECT_EQ(curl_err, translated);
}

class AuthClientTest
  : public ::testing::Test
{
public:
  AuthClientTest(std::string serverprefix)
    : server_(serverprefix + std::string(server))
  {
    ac = auth_client_new("service", server_.c_str());
    assert(ac != NULL);
    auth_client_set_verbose(ac, 1);
  }

  AuthClientTest()
    : AuthClientTest("")
  {}

  virtual ~AuthClientTest() {
    auth_client_free(ac);
  }

  std::string server_;
  auth_client_t ac;
};

TEST_F(AuthClientTest, CertSetupFailsWithoutCA) {
  EXPECT_NE(AC_OK,
            auth_client_set_certificate(ac, "nonexisting.pem", ssl_cert, ssl_key));
  EXPECT_NE(AC_OK,
            auth_client_set_certificate(ac, ssl_ca, "nonexisting.pem", ssl_key));
  EXPECT_NE(AC_OK,
            auth_client_set_certificate(ac, ssl_ca, ssl_cert, "nonexisting.key"));
}

TEST_F(AuthClientTest, AuthOK) {
  int result;

  result = auth_client_set_certificate(ac, ssl_ca, ssl_cert, ssl_key);
  EXPECT_EQ(AC_OK, result) << "set_certificate() error: " << auth_client_strerror(result);

  result = auth_client_authenticate(ac, "user", test_password, NULL, "127.0.0.1", NULL);
  EXPECT_EQ(AC_OK, result) << "authenticate() error: " << auth_client_strerror(result)
                           << ", server=" << server;
}

TEST_F(AuthClientTest, ManyAuthOK) {
  int result;

  for (int i = 0; i < 3; i++) {

    result = auth_client_set_certificate(ac, ssl_ca, ssl_cert, ssl_key);
    EXPECT_EQ(AC_OK, result) << "set_certificate() error: " << auth_client_strerror(result);
    
    result = auth_client_authenticate(ac, "user", test_password, NULL, "127.0.0.1", NULL);
    EXPECT_EQ(AC_OK, result) << "authenticate() error: " << auth_client_strerror(result)
                             << ", server=" << server;
  }

}

TEST_F(AuthClientTest, AuthFail) {
  int result;

  result = auth_client_set_certificate(ac, ssl_ca, ssl_cert, ssl_key);
  EXPECT_EQ(AC_OK, result) << "set_certificate() error: " << auth_client_strerror(result);

  result = auth_client_authenticate(ac, "user", "bad_password", NULL, "127.0.0.1", NULL);
  EXPECT_NE(AC_OK, result) << "authenticate() didn't fail"
                           << ", server=" << server;
}

TEST_F(AuthClientTest, SSLFailsWithBadCertificate) {
  int result;

  // We can't tell auth_client to make an https request without a
  // client certificate, but we can try to force a failure by
  // providing a bad (unloadable) certificate, for example one where
  // the private and public keys do not match. In this case,
  // auth_client_set_certificate() should still succeed, since it
  // doesn't perform this kind of correctness check.
  result = auth_client_set_certificate(ac, ssl_ca, ssl_ca, ssl_key);
  EXPECT_EQ(AC_OK, result) << "set_certificate() error: " << auth_client_strerror(result);

  result = auth_client_authenticate(ac, "user", test_password, NULL, "127.0.0.1", NULL);
  EXPECT_NE(AC_OK, result) << "authenticate() didn't fail, server=" << server;
}

// Test CA validation on the client.
TEST_F(AuthClientTest, SSLFailsWithBadCAClientSide) {
  int result;

  result = auth_client_set_certificate(ac, ssl_bad_ca, ssl_cert, ssl_key);
  EXPECT_EQ(AC_OK, result) << "set_certificate() error: " << auth_client_strerror(result);

  result = auth_client_authenticate(ac, "user", test_password, NULL, "127.0.0.1", NULL);
  EXPECT_NE(AC_OK, result) << "authenticate() didn't fail, server=" << server;
}

// Test CA validation on the server.
TEST_F(AuthClientTest, SSLFailsWithBadCAServerSide) {
  int result;

  result = auth_client_set_certificate(ac, ssl_ca, ssl_bad_cert, ssl_bad_key);
  EXPECT_EQ(AC_OK, result) << "set_certificate() error: " << auth_client_strerror(result);

  result = auth_client_authenticate(ac, "user", test_password, NULL, "127.0.0.1", NULL);
  EXPECT_NE(AC_OK, result) << "authenticate() didn't fail, server=" << server;
}

class AuthClientServerFallbackTest
  : public AuthClientTest
{
public:
  AuthClientServerFallbackTest()
    : AuthClientTest("127.8.8.8:1024,")
  {}
};

// Test RPC fallback if first server is bad.
TEST_F(AuthClientServerFallbackTest, AuthOK) {
  int result;

  result = auth_client_set_certificate(ac, ssl_ca, ssl_cert, ssl_key);
  EXPECT_EQ(AC_OK, result) << "set_certificate() error: " << auth_client_strerror(result);

  result = auth_client_authenticate(ac, "user", test_password, NULL, "127.0.0.1", NULL);
  EXPECT_EQ(AC_OK, result) << "authenticate() error: " << auth_client_strerror(result)
                           << ", server=" << server;
}


int main(int argc, char **argv) {
  server = getenv("AUTH_SERVER");
  if (server == NULL) {
    printf("This test requires a local auth server. Set the AUTH_SERVER environment variable to its address.\n");
    exit(0);
  }

  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
