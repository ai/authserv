import crypt
from werkzeug.security import safe_str_cmp
from authserv.oath import accept_totp
from authserv import protocol


def _check_main_password(userpw, password):
    if safe_str_cmp(crypt.crypt(password, userpw), userpw):
        return protocol.OK, None
    else:
        return protocol.ERR_AUTHENTICATION_FAILURE, 'bad password'


def _check_app_specific_password(asps, password):
    for app_pw in asps:
        if safe_str_cmp(crypt.crypt(password, app_pw), app_pw):
            return protocol.OK, None
    return protocol.ERR_AUTHENTICATION_FAILURE, 'bad app-specific password'


def _check_otp(totp_key, token):
    try:
        ok, drift = accept_totp(totp_key, token, format='dec6',
                                period=30, forward_drift=1,
                                backward_drift=1)
        if ok:
            return protocol.OK
    except:
        pass
    return protocol.ERR_AUTHENTICATION_FAILURE


def authenticate(user, service, password, otp_token, source_ip=None,
                 password_only=False,
                 allow_plain_password_from_localhost=False):
    if not password:
        return protocol.ERR_AUTHENTICATION_FAILURE, 'empty password'

    if isinstance(password, unicode):
        password = password.encode('utf-8')

    if not password_only:
        # If the account has app-specific passwords, do not allow login
        # with the standard password unless it comes from localhost (so
        # that the old-crappy-SSO can successfully pass the password along
        # from the user panel to the webmail). The localhost exception
        # should be removed as soon as that system is replaced.
        if user.app_specific_passwords_enabled():
            result = _check_app_specific_password(
                user.get_app_specific_passwords(service), password)
            # If allow_plain_password_from_localhost is set to False,
            # return the ASP check result in any case. Otherwise fall
            # back to plain password check (if the host is localhost).
            if result == protocol.OK or (
                    not allow_plain_password_from_localhost or source_ip != '127.0.0.1'):
                return result

        # If OTP is enabled for this account, require it along with the
        # right password for a successful login.
        if user.otp_enabled():
            if not otp_token:
                return protocol.ERR_OTP_REQUIRED, 'otp required'
            if _check_otp(user.get_totp_key(), otp_token) != protocol.OK:
                return protocol.ERR_AUTHENTICATION_FAILURE, 'bad otp token'
            return _check_main_password(user.get_password(), password)

    return _check_main_password(user.get_password(), password)
