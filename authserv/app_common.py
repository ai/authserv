import re
from flask import abort, current_app
from authserv import auth
from authserv import protocol
from authserv.ratelimit import *

_user_blacklisted = (protocol.ERR_AUTHENTICATION_FAILURE, 'user_blacklisted', None)
_ip_blacklisted = (protocol.ERR_AUTHENTICATION_FAILURE, 'ip_blacklisted', None)


def check_ratelimit(request, username, source_ip):
    if current_app.config.get('ENABLE_RATELIMIT'):
        if not ratelimit_http_request(
                request, username, tag='u',
                count=current_app.config.get('RATELIMIT_USER_COUNT', 10),
                period=current_app.config.get('RATELIMIT_USER_PERIOD', 60)):
            abort(503)
        if (source_ip
            and not whitelisted(source_ip)
            and not ratelimit_http_request(
                request, source_ip, tag='ip',
                count=current_app.config.get('RATELIMIT_SOURCEIP_COUNT', 10),
                period=current_app.config.get('RATELIMIT_SOURCEIP_PERIOD', 60))):
            abort(503)


def _validate_username(username):
    """Validate a username before fetching it from the backend.

    More of a syntax check than anything. It's just to save cycles in
    case of (only the most trivial of) brute-force / exploitation
    attempts.

    Returns the validated username (type str).
    """
    if not username:
        raise Exception('empty username')

    if len(username) > 256:
        raise Exception('name too long')

    # This will throw a UnicodeEncodeError on non-ASCII usernames.
    username = str(username)

    # Check that it does not contain spaces or newlines.
    if re.match(r'\s', username):
        raise Exception('invalid characters in username')

    return username


def do_auth(username, service, shard, password, otp_token, source_ip,
            password_only=False):
    with current_app.memcache.reserve() as mc:
        return _do_auth(mc, username, service, shard, password, otp_token,
                        source_ip, password_only)


def _do_auth(mc, username, service, shard, password, otp_token, source_ip,
            password_only):
    # Username must be an ASCII string.
    bl = AuthBlackList(current_app.config.get('BLACKLIST_COUNT', 50),
                       current_app.config.get('BLACKLIST_PERIOD', 600),
                       current_app.config.get('BLACKLIST_TIME', 2*3600),
                       mc)
    if current_app.config.get('ENABLE_BLACKLIST'):
        if bl.is_blacklisted('u', username):
            return _user_blacklisted
        is_whitelisted = whitelisted(source_ip)
        if (source_ip and not is_whitelisted
            and bl.is_blacklisted('ip', source_ip)):
            return _ip_blacklisted

    retval = protocol.ERR_AUTHENTICATION_FAILURE
    errmsg = 'user does not exist'
    out_shard = None

    try:
        username = _validate_username(username)
    except:
        return (retval, errmsg, None)

    user = current_app.userdb.get_user(username, service, shard)
    if user:
        retval, errmsg = auth.authenticate(
            user, service, password, otp_token, source_ip, password_only,
            current_app.config.get('ALLOW_PLAIN_PASSWORD_FROM_LOCALHOST', False))
        out_shard = user.get_shard()
        if shard and out_shard != shard:
            retval = protocol.ERR_AUTHENTICATION_FAILURE
            errmsg = 'wrong shard'

    if retval != protocol.OK and current_app.config.get('ENABLE_BLACKLIST'):
        if user:
            if bl.auth_failure('u', username):
                current_app.logger.info('blacklisted user %s', username)
        if source_ip and not is_whitelisted:
            if bl.auth_failure('ip', source_ip):
                current_app.logger.info('blacklisted IP %s', source_ip)

    return (retval, errmsg, out_shard)
