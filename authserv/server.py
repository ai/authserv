import gevent
from gevent.monkey import patch_all
patch_all()
from gevent.pywsgi import WSGIServer

import logging
import logging.handlers
import optparse
import os
import pylibmc
import signal
import sys
from authserv import app_main
from authserv import app_nginx
from authserv import ratelimit


def create_app(app, userdb=None, mc=None, extra_config=None):
    app.config.from_envvar('APP_CONFIG', silent=True)
    if extra_config:
        app.config.update(extra_config)

    if not userdb:
        from authserv import ldap_model
        userdb = ldap_model.UserDb(
            app.config['LDAP_SERVICE_MAP'],
            app.config.get('LDAP_SCHEMA'),
            app.config.get('LDAP_URI', 'ldap://127.0.0.1:389'),
            app.config['LDAP_BIND_DN'],
            app.config['LDAP_BIND_PW'])
    app.userdb = userdb

    if not mc:
        client = pylibmc.Client(
            app.config['MEMCACHE_ADDR'], binary=True)
        mc = pylibmc.ThreadMappedPool(client)
    app.memcache = mc

    ratelimit.init_whitelist(app)

    return app


def run(flask_app, addr, port, ssl_ca, ssl_cert, ssl_key, dh_params):
    ssl_args = {}
    if ssl_ca and os.path.exists(ssl_ca):
        from gevent import ssl
        ssl_args = {
            'server_side': True,
            'certfile': ssl_cert,
            'keyfile': ssl_key,
            'ca_certs': ssl_ca,
            'cert_reqs': ssl.CERT_REQUIRED,
            'ssl_version': ssl.PROTOCOL_TLSv1,
        }
    logging.info('starting gevent server on %s:%d', addr, port)
    # Set log=None to disable the access log.
    WSGIServer((addr, port), flask_app.wsgi_app, log=None, **ssl_args).serve_forever()


def main(sysargs=None):
    if sysargs is None:
        sysargs = sys.argv[1:]
    parser = optparse.OptionParser()
    parser.add_option('--config',
                      help='Configuration file')
    parser.add_option('--addr', default='0.0.0.0',
                      help='Address to listen on (default: %default)')
    parser.add_option('--port', type='int', default=1616,
                      help='TCP port to listen on (default: %default)')
    parser.add_option('--nginx-port', dest='nginx_port', type='int', default=0,
                      help='TCP port for the NGINX mail_http_auth handler '
                      '(forced bind to localhost, default: disabled)')
    parser.add_option('--ca', dest='ssl_ca',
                      default='/etc/ai/internal_ca.pem',
                      help='SSL CA certificate file (default: %default)')
    parser.add_option('--ssl-cert', dest='ssl_cert',
                      default='/etc/ai/localhost_internal.pem',
                      help='SSL client certificate file (default: %default)')
    parser.add_option('--ssl-key', dest='ssl_key',
                      default='/etc/ai/localhost_internal.key',
                      help='SSL client key file (default: %default)')
    parser.add_option('--dhparams', dest='dh_params',
                      default='/etc/ai/dhparams',
                      help='Diffie-Helmann parameters file')
    parser.add_option('--syslog', action='store_true')
    parser.add_option('--debug', action='store_true')

    opts, args = parser.parse_args(sysargs)
    if len(args) != 0:
        parser.error('Too many arguments')

    loglevel = logging.DEBUG if opts.debug else logging.INFO
    if opts.syslog:
        handler = logging.handlers.SysLogHandler(
            address='/dev/log',
            facility=logging.handlers.SysLogHandler.LOG_DAEMON)
        handler.setFormatter(
            logging.Formatter('auth_server: %(message)s'))
        for l in (logging.getLogger(), app_main.app.logger, app_nginx.app.logger):
            l.setLevel(loglevel)
            l.addHandler(handler)
    else:
        logging.basicConfig(level=loglevel,
                            format='%(levelname)s: %(message)s')

    if opts.config:
        os.environ['APP_CONFIG'] = opts.config
    if opts.debug:
        for a in (app_main.app, app_nginx.app):
            a.config['DEBUG'] = True

    def _stopall(signo, frame):
        logging.info('terminating with signal %d', signo)
        os._exit(0)
    gevent.signal(signal.SIGINT, _stopall)
    gevent.signal(signal.SIGTERM, _stopall)

    # Start the applications that were requested: the NGINX
    # mail_http_auth handler (on its own thread), and the main auth
    # server application. Try to catch errors with the application
    # setup and display something nicer than a stack trace.
    if opts.nginx_port > 0:
        try:
            app = create_app(app_nginx.app)
        except Exception as e:
            logging.fatal("Application setup error: %s", e)
            return 1
        gevent.spawn(run, app, '127.0.0.1', opts.nginx_port,
                     None, None, None, None)
    try:
        app = create_app(app_main.app)
    except Exception as e:
        logging.fatal("Application setup error: %s", e)
        return 1
    run(app, opts.addr, opts.port, opts.ssl_ca,
        opts.ssl_cert, opts.ssl_key, opts.dh_params)


if __name__ == '__main__':
    sys.exit(main())
