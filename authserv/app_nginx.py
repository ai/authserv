import socket
import threading
import urllib
from flask import Flask, request, abort, make_response
from authserv.app_common import do_auth, check_ratelimit

app = Flask(__name__)

_default_port_map = {'imap': 143, 'pop3': 110}

_dns_cache = {}
_dns_cache_lock = threading.Lock()

def _shard_to_ip(shard):
    hostname = '%s-vpn' % shard
    with _dns_cache_lock:
        if hostname not in _dns_cache:
            _dns_cache[hostname] = socket.gethostbyname(hostname)
        return _dns_cache[hostname]


@app.route('/', methods=('GET',))
def do_nginx_http_auth():
    service = app.config.get('NGINX_AUTH_SERVICE', 'mail')
    username = urllib.unquote(request.environ.get('HTTP_AUTH_USER'))
    password = urllib.unquote(request.environ.get('HTTP_AUTH_PASS'))
    source_ip = request.environ.get('HTTP_CLIENT_IP')
    protocol = request.environ.get('HTTP_AUTH_PROTOCOL')
    try:
        n_attempt = int(request.environ.get('HTTP_AUTH_LOGIN_ATTEMPT'))
    except ValueError:
        n_attempt = 1

    check_ratelimit(request, username, source_ip)

    try:
        auth_status, errmsg, shard = do_auth(
            username, service, None, password, None, source_ip)
    except Exception, e:
        app.logger.exception('Unexpected exception in authenticate()')
        abort(500)

    log_parts = [username, service, auth_status]
    if shard:
        log_parts.append('-> shard=%s' % shard)
    if errmsg:
        log_parts.append('err=%s' % errmsg)
    app.logger.info('NGINX_AUTH %s', ' '.join(log_parts))

    response = make_response('')
    if auth_status == 'OK':
        response.headers['Auth-Status'] = 'OK'
        response.headers['Auth-Server'] = _shard_to_ip(shard)
        response.headers['Auth-Port'] = str(
            app.config.get('NGINX_AUTH_PORT_MAP', _default_port_map)[protocol])
    else:
        response.headers['Auth-Status'] = 'Invalid login or password'
        if n_attempt <= 3:
            response.headers['Auth-Wait'] = '3'
    return response
