import ipaddr
import re
import threading
from flask import abort, request, current_app
from authserv import protocol


key_sep = '/'

DEFAULT_WHITELIST = [
    '127.0.0.1',
    '::1',
    '172.16.1.0/24',
]


def init_whitelist(app):
    wl = app.config.get('SOURCE_IP_WHITELIST', DEFAULT_WHITELIST)
    app.whitelist = [ipaddr.IPNetwork(x) for x in wl]


_fake_v6_re = re.compile(r'^::\d+\.\d+\.\d+\.\d+$')

def whitelisted(s):
    if not s:
        return False
    # Some endpoints like to pass IPv4 addresses as ::addr, which
    # isn't really entirely correct and confuses ipaddr.
    if _fake_v6_re.match(s):
        s = s[2:]
    ip = ipaddr.IPAddress(s)
    for net in current_app.whitelist:
        if (ip in net) or (hasattr(ip, 'ipv4_mapped') and ip.ipv4_mapped and ip.ipv4_mapped in net):
            return True
    return False


def _tostr(s):
    if isinstance(s, unicode):
        return s.encode('utf-8')
    return s


class RateLimit(object):
    """Keep track of request rates using Memcache."""

    prefix = 'authserv/r:'

    def __init__(self, count, period):
        self.count = count
        self.period = period

    def check(self, mc, key):
        key = _tostr(self.prefix + key)
        try:
            result = mc.incr(key)
        except:
            result = None
        if result is None:
            result = 1
            if not mc.add(key, result, time=self.period):
                try:
                    result = mc.incr(key)
                except:
                    result = None
                if result is None:
                    # Memcache is failing. Fail open.
                    return True
        return result <= self.count


def ratelimit_http_request(req, value, tag='', count=0, period=0):
    """Rate limit an HTTP request handler.

    Returns False if the specified request triggers the rate limit.
    """

    rl = RateLimit(count, period)
    key = key_sep.join([req.method, req.path, value])
    with current_app.memcache.reserve() as mc:
        return rl.check(mc, key)


class BlackList(object):
    """Block requests once a rate limit is triggered."""

    prefix = 'authserv/b/'

    def __init__(self, count, period, ttl):
        self.rl = RateLimit(count, period)
        self.ttl = ttl

    def check(self, mc, key):
        key = _tostr(self.prefix + key)
        result = mc.get(key)
        return result is None

    def incr(self, mc, key):
        # Returns True if the key was added to the blacklist.
        key = _tostr(self.prefix + key)
        if not self.rl.check(mc, key):
            mc.set(key, 'true', time=self.ttl)
            return True
        return False


class AuthBlackList(object):

    def __init__(self, count, period, ttl, mc):
        self.blacklist = BlackList(count, period, ttl)
        self.mc = mc

    def is_blacklisted(self, tag, value):
        if not value:
            return False
        key = key_sep.join((tag, value))
        return not self.blacklist.check(self.mc, key)

    def auth_failure(self, tag, value):
        # Returns True if the key was added to the blacklist.
        if not value:
            return False
        key = key_sep.join((tag, value))
        return self.blacklist.incr(self.mc, key)

