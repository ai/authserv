import abc


class UserDb(object):

    __metaclass__ = abc.ABCMeta

    def get_user(self, username, service, shard):
        pass


class User(object):

    __metaclass__ = abc.ABCMeta

    def app_specific_passwords_enabled(self):
        pass

    def otp_enabled(self):
        pass

    def get_name(self):
        pass

    def get_totp_key(self):
        pass

    def get_app_specific_passwords(self, service):
        pass

    def get_password(self):
        pass

    def get_shard(self):
        pass
