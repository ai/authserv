import contextlib
import ldap
import logging
from ldap.dn import escape_dn_chars
from ldap.filter import escape_filter_chars
from ldap.ldapobject import LDAPObject
from authserv import model


# Define the LDAP schema attributes that we will use.
DEFAULT_SCHEMA = {
    'password': 'userPassword',
    'otp_secret': 'totpSecret',
    'app_specific_password': 'appSpecificPassword',
    'shard': 'host'
}


class Error(Exception):
    pass


def _expandvars(s, vars, quotefn):
    if not s:
        return s
    qvars = dict((k, quotefn(str(v))) for k, v in vars.iteritems())
    return s % qvars


class UserDb(model.UserDb):

    def __init__(self, service_map, schema, ldap_uri, ldap_bind_dn, ldap_bind_pw):
        self.service_map = service_map
        self.schema = DEFAULT_SCHEMA
        if schema:
            self.schema.update(schema)
        self.ldap_attrs = filter(None, self.schema.values())
        self.ldap_uri = ldap_uri
        self.ldap_bind_dn = ldap_bind_dn
        self.ldap_bind_pw = ldap_bind_pw

    @contextlib.contextmanager
    def _conn(self):
        c = LDAPObject(self.ldap_uri)
        c.protocol_version = ldap.VERSION3
        c.simple_bind_s(self.ldap_bind_dn, self.ldap_bind_pw)
        yield c
        c.unbind_s()

    def _query_user(self, username, service, shard):
        # Allow referencing a service to another, by specifying a
        # string rather than a dictionary as the value. If you build
        # infinite loops this way, it's your fault.
        ldap_params = self.service_map.get(service)
        while isinstance(ldap_params, basestring):
            ldap_params = self.service_map.get(ldap_params)
        if not ldap_params:
            logging.error('unknown service "%s"', service)
            return None

        # Arguments used for variable substitution in the LDAP filters.
        ldap_vars = {'user': username,
                     'shard': shard,
                     'service': service}

        with self._conn() as c:
            # LDAP queries can be built in two ways:
            #
            # - specify a fully-qualified 'dn' attribute, in which
            # case we'll run a SCOPE_BASE query for that specific DN.
            # In this case, 'filter' is optional.
            #
            # - specify 'base' and 'filter' together to identify a
            # single object. This is a SCOPE_SUBTREE search and
            # 'filter' is required.
            #
            if 'dn' in ldap_params:
                base = _expandvars(ldap_params['dn'], ldap_vars, escape_dn_chars)
                filt = _expandvars(ldap_params.get('filter', '(objectClass=*)'),
                                   ldap_vars, escape_filter_chars)
                scope = ldap.SCOPE_BASE
            else:
                base = _expandvars(ldap_params['base'], ldap_vars, escape_dn_chars)
                filt = _expandvars(ldap_params['filter'], ldap_vars, escape_filter_chars)
                scope = ldap.SCOPE_SUBTREE
            logging.debug('ldap search: base=%s, scope=%s, filt=%s', base, scope, filt)
            result = c.search_s(base, scope, filt, self.ldap_attrs)

            if not result:
                return None
            if len(result) > 1:
                raise Error('too many results from LDAP')

            return User(username, result[0][0], result[0][1], self.schema)

    def get_user(self, username, service, shard):
        try:
            return self._query_user(username, service, shard)
        except (Error, ldap.LDAPError), e:
            logging.error('userdb error: %s', e)
            return None


class AppSpecificPassword(object):

    def __init__(self, s=None, service=None, enc_password=None,
                 comment=None):
        if s:
            parts = s.split(':', 2)
            self.service = parts[0]
            self.password = parts[1]
            self.comment = parts[2]
        else:
            self.service = service
            self.password = enc_password
            self.comment = comment

    def __str__(self):
        return ':'.join([self.service, self.password, self.comment])


class User(model.User):

    def __init__(self, username, dn, data, schema):
        self._username = username
        self._dn = dn
        self._otp_enabled = False
        self._asps = []
        self._shard = None
        for key, values in data.iteritems():
            if key == schema['password']:
                self._password = values[0]
                if self._password.startswith('{crypt}'):
                    self._password = self._password[7:]
            elif key == schema['otp_secret']:
                self._otp_enabled = True
                self._totp_secret = values[0]
            elif key == schema['app_specific_password']:
                # Format is service:password:comment. Ignore the
                # comment, and avoid dying on malformed input.
                self._asps = []
                for v in values:
                    try:
                        self._asps.append(AppSpecificPassword(v))
                    except:
                        pass
            elif key == schema['shard']:
                self._shard = values[0]

    def otp_enabled(self):
        return self._otp_enabled

    def app_specific_passwords_enabled(self):
        return bool(self._asps)

    def get_name(self):
        return self._username

    def get_totp_key(self):
        return self._totp_secret

    def get_app_specific_passwords(self, service):
        return [x.password for x in self._asps if x.service == service]

    def get_password(self):
        return self._password

    def get_shard(self):
        return self._shard
