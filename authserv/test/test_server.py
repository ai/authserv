import mock
import os
import tempfile
from authserv import server
from authserv.test import *


def unexpected():
    raise Exception('unexpected call')


class ServerTest(unittest.TestCase):

    def setUp(self):
        fd, self.config_file = tempfile.mkstemp()
        os.write(fd, '''
LDAP_SERVICE_MAP = {
  'mail': {
  }
}
LDAP_BIND_DN = 'cn=manager,o=Anarchy'
LDAP_BIND_PW = 'testpass'
MEMCACHE_ADDR = ['127.0.0.1:11211']
''')
        os.close(fd)

    def tearDown(self):
        os.remove(self.config_file)

    def test_run_fails_without_config(self):
        with mock.patch('authserv.server.run', side_effect=unexpected):
            server.main(['--port=1234'])

    def test_run_nossl_ok(self):
        with mock.patch('authserv.server.run') as p:
            server.main(['--config=%s' % self.config_file,
                         '--port=1234'])
            # Don't really look at the SSL params for now.
            p.assert_called_once_with(
                mock.ANY, '0.0.0.0', 1234,
                mock.ANY, mock.ANY, mock.ANY, mock.ANY)
