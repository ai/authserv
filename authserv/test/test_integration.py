import gevent
from gevent import subprocess
import httplib
import os
import socket
import ssl
import sys
import time
import urllib
import urllib2
from authserv.test import *
from authserv.ratelimit import *
from authserv import protocol
from authserv import app_main
from authserv import server

URL = '/api/1/auth'

# A 'difficult' password that requires encoding.
TEST_PASSWORD = 'fagiano& +"% b^rb$g~=|{;\\i'


def _relpath(x):
    return os.path.join(os.path.dirname(__file__), x)


class HTTPSClientAuthHandler(urllib2.HTTPSHandler):

    def __init__(self, cert, key, cafile):
        urllib2.HTTPSHandler.__init__(self, context=ssl.create_default_context(
            purpose=ssl.Purpose.SERVER_AUTH,
            cafile=cafile))
        self.key = key
        self.cert = cert

    def https_open(self, req):
        # Rather than pass in a reference to a connection class, we pass in
        # a reference to a function which, for all intents and purposes,
        # will behave as a constructor
        return self.do_open(self.getConnection, req)

    def getConnection(self, host, timeout=300):
        return httplib.HTTPSConnection(host, key_file=self.key, cert_file=self.cert,
                                       context=self._context)


class SSLServerTest(unittest.TestCase):

    ssl_ca = _relpath('testca/public/ca.pem')
    ssl_cert = _relpath('testca/public/certs/server.pem')
    ssl_key = _relpath('testca/private/server.key')
    client_cert = _relpath('testca/public/certs/client.pem')
    client_key = _relpath('testca/private/client.key')
    dhparams = _relpath('testca/dhparams')

    @classmethod
    def setup_class(cls):
        # set up the server in a separate process.
        cls.users = {
            'user': FakeUser('user', TEST_PASSWORD),
            'user2': FakeUser('user2', TEST_PASSWORD),
        }

        cls.pid = 0
        cls.port = free_port()
        def _runserver():
            app = server.create_app(app_main.app,
                                    userdb=FakeUserDb(cls.users),
                                    mc=FakeMemcachePool(time.time),
                                    extra_config={
                                        'TESTING': True,
                                        'DEBUG': True,
                                    })

            print >>sys.stderr, 'starting HTTP server on port', cls.port
            server.run(
                app, 'localhost', cls.port, cls.ssl_ca,
                cls.ssl_cert, cls.ssl_key, cls.dhparams)

        gevent.spawn(_runserver)
        gevent.sleep(1)

        cls.opener = urllib2.build_opener(
            HTTPSClientAuthHandler(cls.client_cert, cls.client_key, cls.ssl_ca))

    @classmethod
    def teardown_class(cls):
        if cls.pid != 0:
            os.kill(cls.pid, 15)

    def test_python_request_failure_without_cert(self):
        req = urllib2.Request('https://localhost:%d%s' % (self.port, URL),
                              data=urllib.urlencode(
                                  {'username': 'user2',
                                   'password': TEST_PASSWORD,
                                   'service': 'svc',
                                   'source_ip': '127.0.0.1'}))
        self.assertRaises(urllib2.URLError, urllib2.urlopen, req)

    def test_python_auth_simple_ok(self):
        req = urllib2.Request('https://localhost:%d%s' % (self.port, URL),
                              data=urllib.urlencode(
                                  {'username': 'user2',
                                   'password': TEST_PASSWORD,
                                   'service': 'svc',
                                   'source_ip': '127.0.0.1'}))
        resp = self.opener.open(req)
        data = resp.read()
        self.assertEquals(protocol.OK, data)

    def test_auth_client_suite(self):
        # Horrible hack: invoke the auth_client test suite.
        env = dict(os.environ)
        env['AUTH_SERVER'] = 'localhost:%d' % self.port
        retcode = subprocess.call(
            ['make', '-C', _relpath('../../pam'), 'check'],
            env=env)
        class _outputof(object):
            def __init__(self, p):
                self.path = p
            def __str__(self):
                with open(self.path) as fd:
                    return fd.read()
        self.assertEquals(retcode, 0, _outputof(_relpath('../../pam/test-suite.log')))
