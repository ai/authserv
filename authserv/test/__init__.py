import contextlib
import crypt
import logging
import os
import time
import unittest
from ldap_test import LdapServer
from gevent import socket
from authserv import model


class FakeMemcache(object):

    def __init__(self, t=None):
        self.t = t or time.time
        self.data = {}

    def get(self, key):
        result = self.data.get(key)
        if result and result[1] > self.t():
            return result[0]

    def set(self, key, value, time=0):
        self.data[key] = (value, self.t() + time)

    def incr(self, key):
        now = self.t()
        result = self.data.get(key)
        if result and result[1] > now:
            result = (result[0] + 1, result[1])
        else:
            return None
        self.data[key] = result
        return result[0]

    def add(self, key, value, time=0):
        if key in self.data:
            return None
        self.data[key] = (value, self.t() + time)
        return value


class FakeMemcachePool(object):

    def __init__(self, t=None):
        self.mc = FakeMemcache(t)

    @contextlib.contextmanager
    def reserve(self):
        yield self.mc


class FakeUser(model.User):

    def __init__(self, username, password=None, asps=None, otp_key=None, shard=None):
        self.username = username
        self.password = crypt.crypt(password, '$6$abcdef1234567890')
        self.asps = asps
        self.otp_key = otp_key
        self.shard = shard

    def otp_enabled(self):
        return self.otp_key is not None

    def get_totp_key(self):
        return self.otp_key

    def app_specific_passwords_enabled(self):
        return bool(self.asps)

    def get_app_specific_passwords(self, service):
        return [p[1] for p in self.asps if p[0] == service]

    def get_password(self):
        return self.password

    def get_name(self):
        return self.username

    def get_shard(self):
        return self.shard


class FakeUserDb(model.UserDb):

    def __init__(self, users):
        self.users = users

    def get_user(self, username, service, shard):
        return self.users.get(username)


class LdapTestBase(unittest.TestCase):

    LDIFS = []

    @classmethod
    def setup_class(cls):
        # Start the local LDAP server.
        cls.ldap_port = free_port()
        cls.ldap_password = 'testpass'
        ldifs = [os.path.join(os.path.dirname(__file__), 'fixtures', x)
                 for x in ['base.ldif'] + cls.LDIFS]
        logging.getLogger('py4j.java_gateway').setLevel(logging.ERROR)
        cls.server = LdapServer({
            'port': cls.ldap_port,
            'bind_dn': 'cn=manager,o=Anarchy',
            'password': cls.ldap_password,
            'base': {
                'objectclass': ['organization'],
                'dn': 'o=Anarchy',
                'attributes': {'o': 'Anarchy'},
            },
            'ldifs': ldifs,
        })
        cls.server.start()

    @classmethod
    def teardown_class(cls):
        cls.server.stop()


def free_port():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)
    s.bind(('', 0))
    port = s.getsockname()[1]
    s.close()
    return port
