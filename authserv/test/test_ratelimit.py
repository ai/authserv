from authserv.test import *
from authserv.ratelimit import *
from authserv import server
from authserv import app_main


class RateLimitTest(unittest.TestCase):

    def setUp(self):
        self.tick = 0
        def _time():
            return self.tick
        self.mc = FakeMemcachePool(_time)
        self.rl = RateLimit(100, 10)

    def test_ratelimit_pass(self):
        n = 200
        ok = 0
        with self.mc.reserve() as mc:
            for i in xrange(n):
                self.tick = i
                if self.rl.check(mc, 'key'):
                    ok += 1
        self.assertEquals(n, ok)

    def test_ratelimit_fail(self):
        n = 200
        ok = 0
        with self.mc.reserve() as mc:
            for i in xrange(n):
                self.tick = i / 20
                if self.rl.check(mc, 'key'):
                    ok += 1
        self.assertEquals(100, ok)


class BlackListTest(unittest.TestCase):

    def setUp(self):
        self.tick = 0
        def _time():
            return self.tick
        self.mc = FakeMemcache(_time)
        self.bl = BlackList(100, 10, 3600)

    def test_block(self):
        for i in xrange(100):
            self.bl.incr(self.mc, 'key')
            self.assertTrue(self.bl.check(self.mc, 'key'))
        # 101th request is blocked
        self.bl.incr(self.mc, 'key')
        self.assertFalse(self.bl.check(self.mc, 'key'))
        # check expiration of the block
        self.tick = 3601
        self.assertTrue(self.bl.check(self.mc, 'key'))


class WhitelistTest(unittest.TestCase):

    def setUp(self):
        app = server.create_app(app_main.app,
                                userdb=FakeUserDb({}),
                                mc=FakeMemcachePool(),
                                extra_config={
                                    'TESTING': True,
                                    'DEBUG': True,
                                    'SOURCE_IP_WHITELIST': [
                                        '127.0.0.1',
                                        '::1',
                                        '172.16.1.0/24',
                                    ],
                                })
        #self.app = app.test_client()
        self.app = app

    def test_whitelist(self):
        with self.app.app_context():
            self.assertTrue(whitelisted('127.0.0.1'))
            self.assertTrue(whitelisted('::127.0.0.1'))
            self.assertTrue(whitelisted('::ffff:127.0.0.1'))
            self.assertTrue(whitelisted('::1'))
            self.assertTrue(whitelisted('172.16.1.5'))
            self.assertTrue(whitelisted('::172.16.1.5'))
            self.assertTrue(whitelisted('::ffff:172.16.1.5'))

            self.assertFalse(whitelisted('1.2.3.4'))
            self.assertFalse(whitelisted('::1.2.3.4'))
            self.assertFalse(whitelisted('2001:abcd:ef01::14'))
