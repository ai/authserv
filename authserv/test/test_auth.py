from authserv.test import *
from authserv.auth import authenticate
from authserv import protocol
from authserv.oath import totp


class AuthTest(unittest.TestCase):

    def test_main_password_ok(self):
        u = FakeUser('user', 'pass')
        self.assertEquals(
            protocol.OK,
            authenticate(u, 'svc', 'pass', None)[0])

    def test_main_password_fail(self):
        u = FakeUser('user', 'pass')
        self.assertEquals(
            protocol.ERR_AUTHENTICATION_FAILURE,
            authenticate(u, 'svc', 'badpass', None)[0])

    def test_require_otp(self):
        u = FakeUser('user', 'pass', otp_key=1234)
        self.assertEquals(
            protocol.ERR_OTP_REQUIRED,
            authenticate(u, 'svc', 'pass', None)[0])
        self.assertEquals(
            protocol.ERR_OTP_REQUIRED,
            authenticate(u, 'svc', 'badpass', None)[0])

    def test_otp_ok(self):
        secret = '089421'
        token = totp(secret, format='dec6', period=30)
        print 'otp is', token
        u = FakeUser('user', 'pass', otp_key=secret)
        self.assertEquals(
            protocol.OK,
            authenticate(u, 'svc', 'pass', str(token))[0])

    def test_otp_fail(self):
        u = FakeUser('user', 'pass', otp_key='123456')
        self.assertEquals(
            protocol.ERR_AUTHENTICATION_FAILURE,
            authenticate(u, 'svc', 'pass', '123456')[0])
        self.assertEquals(
            protocol.ERR_AUTHENTICATION_FAILURE,
            authenticate(u, 'svc', 'pass', 'malformed otp token!')[0])

    def test_app_specific_password_ok(self):
        u = FakeUser('user', 'pass', asps=[
                ('svc', crypt.crypt('app-specific', 'zz'))])
        self.assertEquals(
            protocol.OK,
            authenticate(u, 'svc', 'app-specific', None)[0])

    def test_app_specific_password_fail_with_main_password(self):
        u = FakeUser('user', 'pass', asps=[
                ('svc', crypt.crypt('app-specific', 'zz'))])
        self.assertEquals(
            protocol.ERR_AUTHENTICATION_FAILURE,
            authenticate(u, 'svc', 'pass', None)[0])
        self.assertEquals(
            protocol.ERR_AUTHENTICATION_FAILURE,
            authenticate(u, 'svc', 'pass', None, '1.2.3.4')[0])

    def test_app_specific_password_with_main_password_from_localhost(self):
        u = FakeUser('user', 'pass', asps=[
                ('svc', crypt.crypt('app-specific', 'zz'))])
        # Result should be OK if the flag is set to True.
        self.assertEquals(
            protocol.OK,
            authenticate(u, 'svc', 'pass', None, '127.0.0.1',
                         allow_plain_password_from_localhost=True)[0])
        # Expect authentication failure if
        # allow_plain_password_from_localhost is False.
        self.assertEquals(
            protocol.ERR_AUTHENTICATION_FAILURE,
            authenticate(u, 'svc', 'pass', None, '127.0.0.1',
                         allow_plain_password_from_localhost=False)[0])

    def test_app_specific_password_fail(self):
        u = FakeUser('user', 'pass', asps=[
                ('svc', crypt.crypt('app-specific', 'zz'))])
        self.assertEquals(
            protocol.ERR_AUTHENTICATION_FAILURE,
            authenticate(u, 'svc', 'badpass', None)[0])
        self.assertEquals(
            protocol.ERR_AUTHENTICATION_FAILURE,
            authenticate(u, 'svc', 'badpass', None, '127.0.0.1')[0])
        self.assertEquals(
            protocol.ERR_AUTHENTICATION_FAILURE,
            authenticate(u, 'svc2', 'app-specific', None)[0])
