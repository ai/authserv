from authserv.test import *
from authserv.ratelimit import *
from authserv.oath import totp
from authserv import protocol
from authserv import server
from authserv import app_main

URL = '/api/1/auth'
URL_PWONLY = '/api/1/auth_pwonly'


class ServerTest(unittest.TestCase):

    def setUp(self):
        self.tick = 0
        def _time():
            return self.tick
        self.users = {
            'user': FakeUser('user', 'pass', shard='a'),
            'otpuser': FakeUser('otpuser', 'pass', otp_key='1234'),
            }
        app = server.create_app(app_main.app,
                                userdb=FakeUserDb(self.users),
                                mc=FakeMemcachePool(_time),
                                extra_config={
                                    'TESTING': True,
                                    'DEBUG': True,
                                    'ENABLE_BLACKLIST': True,
                                    'ENABLE_RATELIMIT': True,
                                    'BLACKLIST_COUNT': 5,
                                    'BLACKLIST_PERIOD': 10,
                                })
        self.app = app.test_client()

    def test_auth_simple_ok(self):
        response = self.app.post(
            URL, data={
                'username': 'user',
                'password': 'pass',
                'service': 'svc'})
        self.assertEquals(protocol.OK, response.data)

    def test_auth_otp_ok(self):
        token = totp('1234', format='dec6', period=30)
        response = self.app.post(
            URL, data={
                'username': 'otpuser',
                'password': 'pass',
                'service': 'svc',
                'otp': token})
        self.assertEquals(protocol.OK, response.data)

    def test_auth_simple_fail(self):
        response = self.app.post(
            URL, data={
                'username': 'user',
                'password': 'badpass',
                'service': 'svc'})
        self.assertEquals(protocol.ERR_AUTHENTICATION_FAILURE,
                          response.data)

    def test_auth_sharded_ok(self):
        response = self.app.post(
            URL, data={
                'username': 'user',
                'password': 'pass',
                'service': 'svc',
                'shard': 'a'})
        self.assertEquals(protocol.OK, response.data)

    def test_auth_sharded_fail(self):
        response = self.app.post(
            URL, data={
                'username': 'user',
                'password': 'pass',
                'service': 'svc',
                'shard': 'b'})
        self.assertEquals(protocol.ERR_AUTHENTICATION_FAILURE,
                          response.data)

    def test_malformed_requests(self):
        bad_data = [
            {'username': 'user'},
            {'username': '', 'service': 'svc'},
            {'username': 'user', 'otp': '1234'},
            ]
        for data in bad_data:
            response = self.app.post(URL, data=data)
            self.assertEquals(400, response.status_code)

    def _create_many_users(self, n):
        for i in xrange(n):
            self.users['user%d' % i] = FakeUser('user%d' % i, 'pass')

    def test_ratelimit_by_client_ip(self):
        n = 20
        ok = 0
        self._create_many_users(n)
        for i in xrange(n):
            response = self.app.post(URL, data={
                'username': 'user%d' % i,
                'password': 'pass',
                'service': 'svc',
                'source_ip': '1.2.3.4'})
            if response.status_code == 200:
                ok += 1
        self.assertEquals(10, ok)

    def test_ratelimit_by_username(self):
        n = 20
        ok = 0
        for i in xrange(n):
            response = self.app.post(URL, data={
                'username': 'user',
                'password': 'pass',
                'service': 'svc',
                'source_ip': '1.2.3.%d' %i})
            if response.status_code == 200:
                ok += 1
        self.assertEquals(10, ok)

    def test_ratelimit_ignores_unset_fields(self):
        # This test will fail if the @ratelimit decorator does not
        # skip its check if one of the fields is unset (in this case,
        # 'source_ip').
        n = 20
        ok = 0
        self._create_many_users(n)
        for i in xrange(n):
            response = self.app.post(URL, data={
                    'username': 'user%d' % i,
                    'password': 'pass',
                    'service': 'svc'})
            if response.status_code == 200:
                ok += 1
        self.assertEquals(n, ok)

    def test_blacklist_by_username(self):
        # Create 6 failed logins.
        for i in xrange(6):
            self.tick += 1
            response = self.app.post(URL, data={
                    'username': 'user', 'password': 'badpass', 'service': 'svc'})
            self.assertEquals(200, response.status_code)
            self.assertEquals(protocol.ERR_AUTHENTICATION_FAILURE, response.data)

        # Now check that all logins fail for at least an hour, even
        # with the right credentials.
        for i in xrange(60):
            self.tick += 60
            response = self.app.post(URL, data={
                    'username': 'user', 'password': 'pass', 'service': 'svc'})
            self.assertEquals(200, response.status_code)
            self.assertEquals(protocol.ERR_AUTHENTICATION_FAILURE, response.data,
                              'failed at %d (t %d)' % (i, self.tick))

        # Expire everything.
        self.tick += 2592000

        # Check that a smaller number of failures does not trigger the
        # blacklist.
        for i in xrange(3):
            self.tick += 1
            response = self.app.post(URL, data={
                    'username': 'user', 'password': 'badpass', 'service': 'svc'})
            self.assertEquals(200, response.status_code)
            self.assertEquals(protocol.ERR_AUTHENTICATION_FAILURE, response.data)

        self.tick += 60
        response = self.app.post(URL, data={
                'username': 'user', 'password': 'pass', 'service': 'svc'})
        self.assertEquals(200, response.status_code)
        self.assertEquals(protocol.OK, response.data)

    def test_blacklist_by_source_ip(self):
        self._create_many_users(60)

        # Create 6 failed logins.
        for i in xrange(6):
            self.tick += 1
            response = self.app.post(URL, data={
                    'username': 'user%d' % i,
                    'password': 'badpass',
                    'service': 'svc',
                    'source_ip': '1.2.3.4'})
            self.assertEquals(200, response.status_code)
            self.assertEquals(protocol.ERR_AUTHENTICATION_FAILURE, response.data)

        # Now check that all logins fail for at least an hour, even
        # with the right credentials.
        for i in xrange(60):
            self.tick += 60
            response = self.app.post(URL, data={
                    'username': 'user%d' % i, 'password': 'pass',
                    'service': 'svc', 'source_ip': '1.2.3.4'})
            self.assertEquals(200, response.status_code)
            self.assertEquals(protocol.ERR_AUTHENTICATION_FAILURE, response.data,
                              'failed at %d (t %d): %s' % (i, self.tick, response.data))

    def test_blacklist_by_source_ip_whitelisted(self):
        self._create_many_users(60)

        # Create 6 failed logins.
        for i in xrange(6):
            self.tick += 1
            response = self.app.post(URL, data={
                    'username': 'user%d' % i,
                    'password': 'badpass',
                    'service': 'svc',
                    'source_ip': '127.0.0.1'})
            self.assertEquals(200, response.status_code)
            self.assertEquals(protocol.ERR_AUTHENTICATION_FAILURE, response.data)

        # Now the next login would fail based on the source_ip match
        # (as proven by the test above), but it should succeed in this
        # case due to 127.0.0.1 being whitelisted.
        self.tick += 1
        response = self.app.post(URL, data={
            'username': 'user1', 'password': 'pass',
            'service': 'svc', 'source_ip': '127.0.0.1'})
        self.assertEquals(200, response.status_code)
        self.assertEquals(protocol.OK, response.data)

    def test_auth_pwonly_simple_ok(self):
        response = self.app.post(
            URL_PWONLY, data={
                'username': 'user',
                'service': 'svc',
                'password': 'pass'})
        self.assertEquals(protocol.OK, response.data)

    def test_auth_pwonly_otp_ok(self):
        response = self.app.post(
            URL_PWONLY, data={
                'username': 'otpuser',
                'service': 'svc',
                'password': 'pass'})
        self.assertEquals(protocol.OK, response.data)

    def test_auth_pwonly_simple_fail(self):
        response = self.app.post(
            URL_PWONLY, data={
                'username': 'user',
                'service': 'svc',
                'password': 'badpass'})
        self.assertEquals(protocol.ERR_AUTHENTICATION_FAILURE,
                          response.data)
