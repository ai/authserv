import mock
from authserv.test import *
from authserv.ratelimit import *
from authserv import protocol
from authserv import server
from authserv import app_nginx


class ServerTest(unittest.TestCase):

    def setUp(self):
        self.tick = 0
        def _time():
            return self.tick
        self.users = {
            'user': FakeUser('user', 'pass'),
            }
        app = server.create_app(app_nginx.app,
                                userdb=FakeUserDb(self.users),
                                mc=FakeMemcachePool(_time),
                                extra_config={
                                    'TESTING': True,
                                    'DEBUG': True,
                                })
        self.app = app.test_client()

    def test_nginx_http_auth_ok(self):
        with mock.patch('socket.gethostbyname', return_value='127.0.0.1'):
            response = self.app.get(
                '/', headers={
                    'Auth-User': 'user',
                    'Auth-Pass': 'pass',
                    'Client-IP': '127.0.0.1',
                    'Auth-Protocol': 'imap',
                    'Auth-Login-Attempt': '1',
                })
            self.assertEquals(200, response.status_code)
            self.assertEquals('OK', response.headers['Auth-Status'])

    def test_nginx_http_auth_fail(self):
        response = self.app.get(
            '/', headers={
                'Auth-User': 'user',
                'Auth-Pass': 'bad password',
                'Client-IP': '127.0.0.1',
                'Auth-Protocol': 'imap',
                'Auth-Login-Attempt': '1',
            })
        self.assertEquals(200, response.status_code)
        self.assertNotEquals('OK', response.headers['Auth-Status'])
