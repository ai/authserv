from authserv.test import *
from authserv.auth import authenticate
from authserv import ldap_model
from authserv import protocol
from authserv.oath import totp


class LdapAuthTestBase(LdapTestBase):

    SERVICE_MAP = {
        'mail': {
            'base': 'ou=People,dc=investici,dc=org,o=Anarchy',
            'filter': '(&(status=active)(mail=%(user)s))',
            },
        'sharded': {
            'base': 'ou=People,dc=investici,dc=org,o=Anarchy',
            'filter': '(&(status=active)(mail=%(user)s)(host=%(shard)s))',
            },
        'account': {
            'dn': 'uid=%(user)s,ou=People,dc=investici,dc=org,o=Anarchy',
            },
        'aliased-service': 'account',
        }

    def setUp(self):
        self.userdb = ldap_model.UserDb(
            self.SERVICE_MAP,
            None,
            'ldap://localhost:%d' % self.ldap_port,
            'cn=manager,o=Anarchy',
            self.ldap_password)


class LdapAuthTest(LdapAuthTestBase):

    LDIFS = [
        'test-user.ldif',
    ]

    def test_userdb_get_user(self):
        self.assertTrue(
            self.userdb.get_user('test@investici.org', 'account', -1))
        self.assertTrue(
            self.userdb.get_user('test@investici.org', 'account', 'whatever'))

    def test_userdb_get_user_sharded(self):
        self.assertTrue(
            self.userdb.get_user('test@investici.org', 'sharded', 'latitanza'))
        self.assertFalse(
            self.userdb.get_user('test@investici.org', 'sharded', 'contumacia'))
        self.assertFalse(
            self.userdb.get_user('test@investici.org', 'sharded', -1))

    def test_userdb_unknown_service(self):
        self.assertFalse(
            self.userdb.get_user('test@investici.org', 'unknownservice', -1))

    def test_userdb_service_alias(self):
        self.assertTrue(
            self.userdb.get_user('test@investici.org', 'aliased-service', -1))

    def test_auth_password_ok(self):
        u = self.userdb.get_user('test@investici.org', 'mail', -1)
        self.assertTrue(u)
        self.assertEquals(
            protocol.OK,
            authenticate(u, 'mail', 'password', None)[0])

    def test_auth_password_fail(self):
        u = self.userdb.get_user('test@investici.org', 'mail', -1)
        self.assertTrue(u)
        self.assertEquals(
            protocol.ERR_AUTHENTICATION_FAILURE,
            authenticate(u, 'mail', 'wrong password', None)[0])


class LdapOtpTest(LdapAuthTestBase):

    LDIFS = [
        'test-user-totp.ldif',
    ]

    def test_auth_password_requires_otp(self):
        u = self.userdb.get_user('test@investici.org', 'account', -1)
        self.assertTrue(u)
        self.assertEquals(
            protocol.ERR_OTP_REQUIRED,
            authenticate(u, 'account', 'password', None)[0])

    def test_auth_bad_password_requires_otp(self):
        u = self.userdb.get_user('test@investici.org', 'account', -1)
        self.assertTrue(u)
        self.assertEquals(
            protocol.ERR_OTP_REQUIRED,
            authenticate(u, 'account', 'wrong password', None)[0])

    def test_auth_otp_ok(self):
        u = self.userdb.get_user('test@investici.org', 'account', -1)
        self.assertTrue(u)
        secret= '089421'
        token = totp(secret, format='dec6', period=30)
        self.assertEquals(
            protocol.OK,
            authenticate(u, 'account', 'password', str(token))[0])

    def test_auth_otp_ok_bad_password(self):
        u = self.userdb.get_user('test@investici.org', 'account', -1)
        self.assertTrue(u)
        secret= '089421'
        token = totp(secret, format='dec6', period=30)
        self.assertEquals(
            protocol.ERR_AUTHENTICATION_FAILURE,
            authenticate(u, 'account', 'wrong password', str(token))[0])

    def test_auth_bad_otp(self):
        u = self.userdb.get_user('test@investici.org', 'account', -1)
        self.assertTrue(u)
        self.assertEquals(
            protocol.ERR_AUTHENTICATION_FAILURE,
            authenticate(u, 'account', 'password', '123456')[0])


class LdapASPTest(LdapAuthTestBase):

    LDIFS = [
        'test-user-totp.ldif',
    ]

    def test_app_specific_password_ok(self):
        u = self.userdb.get_user('test@investici.org', 'mail', -1)
        self.assertTrue(u)
        self.assertEquals(
            protocol.OK,
            authenticate(u, 'mail', 'veryspecificpassword', None)[0])

    def test_plain_password_fails(self):
        u = self.userdb.get_user('test@investici.org', 'mail', -1)
        self.assertTrue(u)
        self.assertEquals(
            protocol.ERR_AUTHENTICATION_FAILURE,
            authenticate(u, 'mail', 'password', None)[0])

    def test_plain_password_and_otp_fails(self):
        u = self.userdb.get_user('test@investici.org', 'mail', -1)
        self.assertTrue(u)
        self.assertEquals(
            protocol.ERR_AUTHENTICATION_FAILURE,
            authenticate(u, 'mail', 'password', '123456')[0])
