from flask import Flask, request, abort, make_response
from authserv import auth
from authserv import protocol
from authserv.app_common import do_auth, check_ratelimit

app = Flask(__name__)


# Quick clarification on the rate limits: 'username' is the one that's
# going to be used all the time, while the X-Forwarded-For header on
# the request is only going to be present for those authentication
# requests where we have knowledge of the original users' IP (remember
# that 'source_ip' can sometimes be the server address or localhost).
# For instance, authentication requests that come from PAM usually do
# not have knowledge of the users' IP address, as the protocols for
# which we use PAM handlers do not support forwarding of the IP
# address. So we're practically only going to use X-Forwarded-For for
# requests that reach our frontends via HTTP.
@app.route('/api/1/auth', methods=('POST',))
def api_auth():
    service = request.form.get('service')
    username = request.form.get('username')
    password = request.form.get('password')
    otp_token = request.form.get('otp')
    source_ip = request.form.get('source_ip')
    shard = request.form.get('shard')

    if not service or not username:
        abort(400)

    check_ratelimit(request, username, source_ip)

    try:
        auth_status, errmsg, unused_shard = do_auth(
            username, service, shard, password, otp_token, source_ip)
    except Exception, e:
        app.logger.exception('Unexpected exception in authenticate()')
        abort(500)

    # Build a nice log message.
    log_parts = [username, service, auth_status]
    log_parts.append('otp=%s' % (otp_token and 'y' or 'n'))
    if shard:
        log_parts.append('shard=%s' % shard)
    if errmsg:
        log_parts.append('err=%s' % errmsg)
    app.logger.info('AUTH %s', ' '.join(log_parts))

    response = make_response(auth_status)
    response.headers['Cache-Control'] = 'no-cache'
    response.headers['Content-Type'] = 'text/plain'
    response.headers['Expires'] = '-1'
    return response


@app.route('/api/1/auth_pwonly', methods=('POST',))
def api_auth_pwonly():
    service = request.form.get('service')
    username = request.form.get('username')
    password = request.form.get('password')

    if not service or not username:
        abort(400)

    try:
        auth_status, errmsg, unused_shard = do_auth(
            username, service, None, password, None, None,
            password_only=True)
    except Exception, e:
        app.logger.exception('Unexpected exception in auth_pwonly()')
        abort(500)

    response = make_response(auth_status)
    response.headers['Cache-Control'] = 'no-cache'
    response.headers['Content-Type'] = 'text/plain'
    response.headers['Expires'] = '-1'
    return response

